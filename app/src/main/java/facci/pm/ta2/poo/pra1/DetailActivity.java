package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    TextView title;
    TextView price;
    ImageView thumbnail;
    TextView descripcion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        String object_id =getIntent().getStringExtra("object_id");
        TextView description = (TextView) findViewById(R.id.descripcion);
        description.setMovementMethod(LinkMovementMethod.getInstance());

        DataQuery dataQuery = DataQuery.get("item");
        dataQuery.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {

                if (e == null){
                    title = (TextView)findViewById(R.id.name);
                    price = (TextView)findViewById(R.id.price);
                    thumbnail = (ImageView)findViewById(R.id.thumbnail);
                    descripcion = (TextView)findViewById(R.id.descripcion);


                    title.setText((String) object.get("name"));
                    price.setText((String) object.get("price")+"\u0024");
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                    descripcion.setText((String) object.get("descripcion"));
                }else{

                }
            }
        });


        // FIN - CODE6

    }

}
